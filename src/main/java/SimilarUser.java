import java.sql.SQLException;
import java.util.Scanner;

public class SimilarUser {
    public static void main(String []args) throws ClassNotFoundException, SQLException {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.print("Enter the user for restaurants to be recommended to:");
            int i = sc.nextInt();

            Vector vg = new Vector();
            CosineSimilarity c = new CosineSimilarity();

            UserCount uc = new UserCount();

            double result = 0;

            Integer[] activeUser = vg.arrayGenerator(i);
            int noOfUsers = uc.returnCount();

            double maxPears = 0;
            int maxSimUser = i;

            for (int j = 1; j <= noOfUsers; j++) {
                if (j != i) {
                    Integer[] otherUser = vg.arrayGenerator(j);

                    result = c.cosineSimilarity(activeUser, otherUser);

                    if (maxPears < result) {
                        maxPears = result;
                        maxSimUser = j;
                    }

                    // System.out.println("Similarity with user "+j+" ="+result);}
                }
            }

            System.out.println("\nMost similar user is User:" + maxSimUser);
            //System.out.println("\nFor User " + i + " Recommended Books are: \n");

            Integer[] similarUser = vg.arrayGenerator(maxSimUser);
            Recommendation rm = new Recommendation();
            Object[] recommendedBooks = rm.recommendedRestaurant(activeUser, similarUser);

            System.out.println("Recommended restaurants for user " + i + " are:");
            for (int x = 0; x < recommendedBooks.length; x++) {
                System.out.print(recommendedBooks[x] + " ");
            }
        }
        catch (ClassNotFoundException c){
            System.out.println(c);
        }
        catch (SQLException s){
            System.out.println(s);
        }
    }
}
