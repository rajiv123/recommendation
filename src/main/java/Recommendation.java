import java.util.ArrayList;
import java.util.List;

public class Recommendation {
    public Object [] recommendedRestaurant(Integer []activeUser, Integer []similarUser){

        List<Integer> unratedRestaurantList = new ArrayList<>();
        List <Integer> recommendationsList = new ArrayList<>();

        for(int i = 0 ; i<activeUser.length ; i++){
            if(activeUser[i] == 0){
                unratedRestaurantList.add(i);
            }
        }

        Integer []restaurantList = new Integer[activeUser.length];

        for(int i = 0; i<activeUser.length ; i++){

            restaurantList[i] = i+1;
        }

        for(Integer unratedRestaurant: unratedRestaurantList ) {
            if(1 <= similarUser[unratedRestaurant] ) {
                recommendationsList.add(restaurantList[unratedRestaurant]);
            }

        }

        Object []recommendationsListArray = recommendationsList.toArray();

        return recommendationsListArray;

    }
}
