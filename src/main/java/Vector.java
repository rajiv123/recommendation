import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Vector {
    public Integer [] arrayGenerator(double userID) throws ClassNotFoundException, SQLException {

        Class.forName("com.mysql.jdbc.Driver");

        Connection con = DriverManager.getConnection("jdbc:mysql://localhost/project","root","");


        String sql = "call rating('"+userID+"')";
        PreparedStatement stmt = con.prepareStatement(sql);

         ResultSet rs = stmt.executeQuery(String.format(sql, "rating"));

        List<String> user = new ArrayList<>();
        while(rs.next()) {
            user.add(rs.getString(2));
        }


        String []userArr = (String[]) user.toArray(new String[user.size()]);

        Integer []user1Arr = processLine(userArr);



        return user1Arr;
    }

    public void displayArray(Integer []Arr){
        for(int i = 0; i<Arr.length; i++){
            System.out.print(Arr[i]+" ");
        }
    }


    private static Integer [] processLine(String[] strings) {
        Integer [] intarray=new Integer[strings.length];
        int i=0;
        for(String str:strings){
            try {
                intarray[i]=Integer.parseInt(str);
                i++;
            }
            catch (NumberFormatException e) {
                throw new IllegalArgumentException("Not a number: " + str + " at index " + i, e);
            }
        }
        return intarray;
    }
}
